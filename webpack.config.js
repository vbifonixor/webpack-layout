const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

const extractCSS = new ExtractTextPlugin({
  filename: "../css/bundle.css"
})

module.exports = {
  entry: './src/js/main.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist/js')
  },
  plugins: [ extractCSS ],
  devtool: 'inline-source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env', 'es2015', 'es2016'],
            minified: true,
          }
        }
      },
      {
        test: /\.css$/,
        use: extractCSS.extract( {
          use: [
            { loader: 'css-loader', options: {
              sourceMap: true,
              importLoaders: 1
            } },
            { loader: 'postcss-loader', options: {
                config: { path: 'postcss.config.js' }
              }
            },
          ]
        })
    }]
  }
}
