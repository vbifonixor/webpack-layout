module.exports = {
  parser: 'postcss-scss',
  plugins: {
    'postcss-import': {},
    'stylelint': {
      syntax: 'scss'
    },
    'precss': {},
    'postcss-browser-reporter': {},
    'autoprefixer': {},
    // 'cssnano': {},
    'postcss-base64': {
      extensions: ['.svg']
    },
  },
  sourceMap: 'inline'
}
